#
# This code was used to produce the heatmaps for the normal steady states
# in the gut microbiome model and the effects of perturbations (Figure 3).
#
#

import boolean2, pylab
from boolean2 import util
import pandas as pd
from pandas import Series, DataFrame
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LinearSegmentedColormap

nodeNames = ["Akkermansia",
"Barnesiella",
"Blautia",
"Clindamycin",
"Clostridium_difficile",
"Coprobacillus",
"Enterobacteriaceae",
"Enterococcus",
"Lachnospiraceae",
"Lachnospiraceae_other",
"Mollicutes",
"Other"]

icHealthy = """
# Healthy (Pop1) Initial condition
Akkermansia = False
Barnesiella = True
Blautia = False
Clindamycin = False
Clostridium_difficile = False
Coprobacillus = False
Enterobacteriaceae = False
Enterococcus = False
Lachnospiraceae = True
Lachnospiraceae_other  = True
Mollicutes  = False
Other  = True
"""

icClinda = """
## Clinda tx (Pop2) rules
Akkermansia = False
Barnesiella = False
Blautia = False
Clindamycin = True
Clostridium_difficile = False
Coprobacillus = False
Enterobacteriaceae = False
Enterococcus = False
Lachnospiraceae = False
Lachnospiraceae_other  = False
Mollicutes  = False
Other  = False

"""
icClindaNoClinda = """
## Clinda tx (Pop2) rules
Akkermansia = False
Barnesiella = False
Blautia = False
Clindamycin = False
Clostridium_difficile = False
Coprobacillus = False
Enterobacteriaceae = False
Enterococcus = False
Lachnospiraceae = False
Lachnospiraceae_other  = False
Mollicutes  = False
Other  = False

"""

icCdiff = """
# Pop3 rules
Akkermansia = False
Barnesiella = False
Blautia = True
Clindamycin = True
Clostridium_difficile = True
Coprobacillus = False
Enterobacteriaceae = True
Enterococcus = True
Lachnospiraceae = False
Lachnospiraceae_other  = False
Mollicutes  = True
Other  = False

"""

icCdiffNoClinda = """
# Pop3 rules
Akkermansia = False
Barnesiella = False
Blautia = True
Clindamycin = False
Clostridium_difficile = True
Coprobacillus = False
Enterobacteriaceae = True
Enterococcus = True
Lachnospiraceae = False
Lachnospiraceae_other  = False
Mollicutes  = True
Other  = False

"""

rules2 = """
Akkermansia* = (Coprobacillus) 
Blautia* = ((Enterococcus) or (Blautia)) or ((not Blautia and Coprobacillus) or (Blautia and not Coprobacillus))
Clindamycin* = (Clindamycin) 
Clostridium_difficile* = (Clostridium_difficile and not Barnesiella)
Coprobacillus* = False
Enterobacteriaceae* = (Enterobacteriaceae) 
### removed the 'and Enterococcus' self loop and the 'not coprobacillus'
#Enterococcus* = (Mollicutes) or (Enterobacteriaceae) or (Clostridium_difficile) or (Blautia) or (not Coprobacillus)
Enterococcus* = (Mollicutes) or (Enterobacteriaceae) or (Clostridium_difficile) or (Blautia)
#Enterococcus* = (Enterococcus and Mollicutes) or (Enterobacteriaceae and Enterococcus) or (not Coprobacillus and Enterococcus) or (Clostridium_difficile and Enterococcus) or (Blautia and Enterococcus)
Mollicutes* = (Mollicutes) 

#Lachnospiraceae* = (Other) or (Lachnospiraceae_other) or (Lachnospiraceae)  or (not Clindamycin) 
#Lachnospiraceae_other* = (Other) or (Lachnospiraceae_other) or (Lachnospiraceae) or (not Clindamycin) 
#Other* = (Other) or (Lachnospiraceae_other) or (Lachnospiraceae) or (not Clindamycin)

Barnesiella* = (Other or Lachnospiraceae_other or Lachnospiraceae) and not Clindamycin
Lachnospiraceae* = (Other or Lachnospiraceae_other or Lachnospiraceae)  and not Clindamycin
Lachnospiraceae_other* = (Other or Lachnospiraceae_other or Lachnospiraceae)  and not Clindamycin
Other* = (Other or Lachnospiraceae_other or Lachnospiraceae)  and not Clindamycin
"""

#save_string = 'icClinda-nodes=ON'
save_string = 'icClindaNoClinda-nodes=ON'
#save_string = 'icClinda-nodes=OFF'
#save_string = 'icClindaNoClinda-nodes=OFF'
#save_string = 'icCdiff-nodes=ON'
#save_string = 'icCdiffNoClinda-nodes=ON'
#save_string = 'icCdiff-nodes=OFF'
#save_string = 'icCdiffNoClinda-nodes=OFF'

rulesPlusIC = icClindaNoClinda + rules2

steady_states = {' Healthy': {'010000001101'}, ' Clindamycin': {'000100000000'}, ' Clindamycin+C. difficile': {'001110110010'}}
steadyStateByKO_dict = {}
for node in nodeNames:

    # these nodes will be overexpressed (initialized to True)
    on  = [node]
    

    # these nodes will be set to false and their corresponding updating 
    # rules will be  removed
    off = []
    
    # this modifies the original states to apply to overexpressed and knockouts
    mod_text = boolean2.modify_states(rulesPlusIC, turnon=on, turnoff=off)
    
    # Number of times to repeat simulations
    r= 100
    # Number of timesteps to run
    s= 12
    coll  = util.Collector()
    for i in range(r):
        print "iteration: " + str(i)
        model = boolean2.Model( mod_text, mode='async')
        model.initialize()
        model.iterate( steps=s ) 
        # in this case we take all nodes
        # one could just list a few nodes such as [ 'A', 'B', 'C' ]
        nodes = model.nodes
        # this collects states for each run
        coll.collect( states=model.states, nodes=nodes )
    
    # this step averages the values for each node
    # returns a dictionary keyed by nodes and a list of values
    # with the average state for in each timestep
    avgs = coll.get_averages( normalize=True )

    simulation_dict = {}
    for node2 in nodeNames:
        simulation_dict[node2] = avgs[node2]
    df = DataFrame(simulation_dict)
    new_df = df.transpose()
    # create a dictionary of all the steady states for each KO produced by model
    steadyStateByKO_dict[node] = new_df[10]

## add the steady states to the dictionary
for ss in steady_states:
    tmpList = list(steady_states[ss])
    tmpList2 = list(tmpList[0])
    tmp = Series(tmpList2, index= nodeNames)
    steadyStateByKO_dict[ss] = tmp

ss_df = DataFrame(steadyStateByKO_dict)

# re-oder columns
cols = [u' Healthy',
 ' Clindamycin',
 ' Clindamycin+C. difficile',
 'Akkermansia',
 'Barnesiella',
 'Blautia',
 'Clindamycin',
 'Clostridium_difficile',
 'Coprobacillus',
 'Enterobacteriaceae',
 'Enterococcus',
 'Lachnospiraceae',
 'Lachnospiraceae_other',
 'Mollicutes',
 'Other']
ss_df = ss_df[cols]
column_labels = list(ss_df.index)
row_labels = list(ss_df.columns)
#plot it
fig, ax = plt.subplots()

ss_df.to_csv("SS_KO_Heatmap.txt", sep='\t')

# Format

# turn off the frame
ax.set_frame_on(False)

#turn off the grid
ax.grid(True)

# put the major ticks at the middle of each cell
ax.set_yticks(np.arange(ss_df.shape[0]) + 0.5, minor=False)
ax.set_xticks(np.arange(ss_df.shape[1]) + 0.5, minor=False)

# want a more natural, table-like display

ax.set_xticklabels(row_labels, minor=False, rotation=90, fontname = 'Cambria',fontsize=14)
ax.set_yticklabels(column_labels, minor=False, fontname = 'Cambria', fontsize=16)
ax.set_ylabel('Node state', fontsize=16, fontname = 'Cambria')
ax.set_xlabel('Steady states', fontsize=16, fontname = 'Cambria')
ax.xaxis.labelpad = 50
ax.invert_yaxis()

# Turn off all the ticks
for t in ax.xaxis.get_major_ticks():
    t.tick1On = False
    t.tick2On = False
for t in ax.yaxis.get_major_ticks():
    t.tick1On = False
    t.tick2On = False

vmax = 1.0
customCmap = LinearSegmentedColormap.from_list('mycmap', [(0 / vmax, 'blue'),(1 / vmax, 'yellow')])
 
plt.subplots_adjust(left=0.38, right=0.95, top=0.95, bottom=0.4)    
heatmap = ax.pcolor(ss_df, cmap=customCmap, alpha=1, vmin=0, vmax=1)

cbar = plt.colorbar(heatmap)
cbar.set_label('Genus Abundance', fontsize=14,fontname = 'Cambria')

print" saved heatmap as: " + save_string
pylab.savefig("./" + save_string + '.png')
plt.show()


##########################################
# Plot heatmap of the normal steady states
##########################################

ssDict = {}
for ss in steady_states:
    tmpList = list(steady_states[ss])
    tmpList2 = list(tmpList[0])
    tmp = Series(tmpList2, index= nodeNames)
    ssDict[ss] = tmp
save_string = 'NormalSSHeatmap'
ss_df2 = DataFrame(ssDict)
cols = [' Healthy',' Clindamycin', ' Clindamycin+C. difficile']
ss_df2 = ss_df2[cols]
column_labels = list(ss_df2.index)
row_labels = [' Healthy',' Clindamycin', ' Clindamycin+\nC. difficile']
fig, ax = plt.subplots()

ss_df2.to_csv("Normal_SS_Heatmap.txt", sep='\t')

# turn off the frame
ax.set_frame_on(True)
#turn off the grid
ax.grid(True)

# put the major ticks at the middle of each cell
ax.set_yticks(np.arange(ss_df2.shape[0]) + 0.5, minor=False)
ax.set_xticks(np.arange(ss_df2.shape[1]) + 0.5, minor=False)

ax.set_xticklabels(row_labels, minor=False, fontname = 'Cambria', fontsize=14, rotation=45)
ax.set_yticklabels(column_labels, minor=False, fontname = 'Cambria')
ax.set_ylabel('Node state', fontsize=16, fontname = 'Cambria')
ax.set_xlabel('Steady states', fontsize=16, fontname = 'Cambria')
ax.set_frame_on(False)
ax.invert_yaxis()

# Turn off all the ticks
for t in ax.xaxis.get_major_ticks():
    t.tick1On = False
    t.tick2On = False
for t in ax.yaxis.get_major_ticks():
    t.tick1On = False
    t.tick2On = False
 

plt.subplots_adjust(left=0.3, right=0.7, top=0.85, bottom=0.2)    
heatmap = ax.pcolor(ss_df2, cmap=customCmap, alpha=1, vmin=0, vmax=1)


cbar = plt.colorbar(heatmap)
cbar.set_label('Genus Abundance', fontsize=14,fontname = 'Cambria')
save_string = "KO_SS_Plot-" + "s=" + str(s) + "-r=" + str(r)

if len(on) > 0:
    save_string += "-ON=" + ''.join(on)
if len(off) > 0:
    save_string += "-OFF=" + ''.join(off)
  
print" saved heatmap as: " + save_string

pylab.savefig("./" + save_string + '.png')
plt.show()

