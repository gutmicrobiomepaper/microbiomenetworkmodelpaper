clear all
close all

OD = dlmread('..\spent_media_Barnesiella_Cdiff_raw_data_6Feb2015.tsv','\t');

%----------------------------------------------------------
time = OD(:,1);
% Cdiff_Fresh_Media
    wells = [2:12:86 3:12:87] + 1;
    cd_fresh = OD(:,wells);
    cd_fresh = processCurves(cd_fresh,1,0.01,0);
    [maxGRs_cd_fresh, Ks_cd_fresh, n0s_cd_fresh, LVGRs_cd_fresh] = multiGrowthRates_LV(time,cd_fresh);
% Binte_Fresh_Media
    wells = [4:12:88 5:12:89] + 1;
    bi_fresh = OD(:,wells);
    bi_fresh = processCurves(bi_fresh,1,0.01,0);
    [maxGRs_bi_fresh, Ks_bi_fresh, n0s_bi_fresh, LVGRs_bi_fresh] = multiGrowthRates_LV(time,bi_fresh);
% Cdiff_Binte_Fresh_Media
    wells = [6:12:90 7:12:91] + 1;
    cdbi_fresh = OD(:,wells);
    cdbi_fresh = processCurves(cdbi_fresh,1,0.02,0);
    [maxGRs_cdbi_fresh, Ks_cdbi_fresh, n0s_cdbi_fresh, LVGRs_cdbi_fresh] = multiGrowthRates_LV(time,cdbi_fresh);
% Cdiff_Bi_Spent_Media
    wells = [8:12:92 9:12:93] + 1;
    cd_bispent = OD(:,wells);
    cd_bispent = processCurves(cd_bispent,1,0.01,0);
    [maxGRs_cd_bispent, ~, ~, LVGRs_cd_bispent] = multiGrowthRates_LV(time,cd_bispent);
% Cdiff_Bi_Spent_Autoclaved_Media
    wells = [10:12:94 11:12:95] + 1;
    cd_biAspent = OD(:,wells);
    cd_biAspent = processCurves(cd_biAspent,1,0.01,0);
    [maxGRs_cd_biAspent, ~, ~, LVGRs_cd_biAspent] = multiGrowthRates_LV(time,cd_biAspent);
%----------------------------------------------------------
 
% Check simply additive model of Cdiff + Binte co-culture
global K1 K2 r1 r2 a12 a21 
N0 = [mean(n0s_cd_fresh) mean(n0s_bi_fresh)];
K1 = mean(Ks_cd_fresh);
K2 = mean(Ks_bi_fresh);
r1 = mean(LVGRs_cd_fresh);
r2 = mean(LVGRs_bi_fresh);
a12 = 0;
a21 = 0;

t = time;
[t,N] = ode45(@dndt,t,N0);

% Calculate max growth rate of co-culture
cdbi_sim = sum(N,2);
[maxGRs_cdbi_sim, ~, ~, LVGRs_cdbi_sim] = multiGrowthRates_LV(time,cdbi_sim);

% Plot Max Growth Rates
figure(1);
bar([mean(maxGRs_cd_fresh) mean(maxGRs_bi_fresh) mean(maxGRs_cdbi_fresh) maxGRs_cdbi_sim mean(maxGRs_cd_bispent) mean(maxGRs_cd_biAspent)]); 
title('Max Growth Rates');

% Growth Curves
figure(2);
maxOD = max(max([cd_fresh bi_fresh cdbi_fresh cd_bispent cd_biAspent]));
subplot(1,5,1); plot(time,cd_fresh); axis([0 max(time)+2 0 maxOD]); 
subplot(1,5,2); plot(time,bi_fresh); axis([0 max(time)+2 0 maxOD]); 
subplot(1,5,3); plot(time,cdbi_fresh); axis([0 max(time)+2 0 maxOD]); title('Growth Curves for Bacteria'); 
subplot(1,5,4); plot(time,cd_bispent); axis([0 max(time)+2 0 maxOD]);
subplot(1,5,5); plot(time,cd_biAspent); axis([0 max(time)+2 0 maxOD]); 

% Area under the curve
auc_cd_fresh = [];
auc_bi_fresh = [];
auc_cdbi_fresh = [];
auc_cd_bispent = [];
auc_cd_biAspent = [];
for i = 1:size(cd_fresh,2)
    auc_cd_fresh(i) = trapz(time,cd_fresh(:,i));
    auc_bi_fresh(i) = trapz(time,bi_fresh(:,i));
    auc_cdbi_fresh(i) = trapz(time,cdbi_fresh(:,i));
    auc_cd_bispent(i) = trapz(time,cd_bispent(:,i));
    auc_cd_biAspent(i) = trapz(time,cd_biAspent(:,i));
end
auc_cdbi_sim = trapz(time,cdbi_sim);
bar([mean(auc_cd_fresh) mean(auc_bi_fresh) mean(auc_cdbi_fresh) auc_cdbi_sim mean(auc_cd_bispent)]); 
title('AUC');

figure(3)
cdbi_fresh_mean = mean(cdbi_fresh,2);
cdbi_fresh_stdev = std(cdbi_fresh,1,2);
errorbar(t(1:15:length(t)),cdbi_fresh_mean(1:15:length(t)),cdbi_fresh_stdev(1:15:length(t)),'ro');
hold on;
plot(t,sum(N,2),'--');
legend('Data Average','Cd+Bi Sim')
xlabel('Time')
ylabel('OD870')

% Write growth rates to file
dlmwrite('spent_media_Barnesiella_Cdiff_growth_rates_6Feb2015.tsv',[maxGRs_cd_fresh;maxGRs_bi_fresh;maxGRs_cdbi_fresh;maxGRs_cd_bispent;maxGRs_cd_biAspent;repmat(maxGRs_cdbi_sim,[1,length(maxGRs_cdbi_fresh)])],'\t');
% Write AUCs to file
dlmwrite('spent_media_Barnesiella_Cdiff_area_under_curves_6Feb2015.tsv',[auc_cd_fresh;auc_bi_fresh;auc_cdbi_fresh;auc_cd_bispent;auc_cd_biAspent;repmat(auc_cdbi_sim,[1,length(auc_cd_biAspent)])],'\t');
% Write growth curves to file
dlmwrite('spent_media_Barnesiella_Cdiff_raw_data_6Feb2015.tsv',OD,'\t');

% Write co-culture growth curves (simulated and experimental) to file
coculture_growth_curves = [time sum(N,2) cdbi_fresh_mean cdbi_fresh_stdev];
colNames = {'Time','Simulated','Mean','Stdev'};
t = sprintf('%s\t',colNames{:});
t(end) = '';
dlmwrite('spent_media_Barnesiella_Cdiff_growth_curves_6Feb2015.tsv',t,'');
dlmwrite('spent_media_Barnesiella_Cdiff_growth_curves_6Feb2015.tsv',coculture_growth_curves,'-append','delimiter','\t');
