function [newYs] = processCurves(oldYs,nNs,start,finish)
% OldYs should have each curve as a separate column

cols = size(oldYs,2);

for i = 1:cols
    newYs(:,i) = oldYs(:,i);
    if nNs > 0
        newYs(:,i) = normalizeAndSmooth(newYs(:,i));
    end
end

end