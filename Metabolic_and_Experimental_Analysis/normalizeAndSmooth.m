function [newY] = normalizeAndSmooth(oldY)

% Normalize by moving the minimum value in the curve to equal 0.001
newY = oldY - min(oldY) + 0.001;
newY = smooth(newY);

end