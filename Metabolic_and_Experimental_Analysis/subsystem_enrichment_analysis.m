% Enrichment analysis to compare subsystem content between subsets 
% of the community. Here, we use KEGG map names as subsystem IDs.
%
% Written by Matt Biggs, January 2015

load genus_model_list
load SEEDrxns2KEGGmaps

%               1               2           3           4                        5              6                   7               8               9      
genus_names = {'Akkermansia' 'Barnesiella' 'Blautia' 'Clostridium_difficile' 'Coprobacillus' 'Enterobacteriaceae' 'Enterococcus' 'Lachnospiraceae' 'Mollicutes'};
numGenera = length(genus_names);

%---------------------------------------------
% Enrichment analysis for the two subnetworks
%---------------------------------------------
% Pool all reactions and count subsystems
rxns = cell(0,1);
for i = 1:numGenera
   rxns = [rxns(:); genera_models{i}.rxns]; 
end
urxns = unique(rxns);

subsys = cell(size(urxns));
for i = 1:length(urxns)
   index = find(strfind_cellarray(SEEDrxns2KEGGmaps.Reaction,urxns{i}));
   subsys{i} = SEEDrxns2KEGGmaps.KeggMaps{index};
end

allSubsystems = cell(0,1);
for i = 1:length(subsys)
    curSubsystems = strsplit(subsys{i},'|');
    allSubsystems = [allSubsystems(:); curSubsystems(:)]; 
end
usubsys = unique(allSubsystems);
totalSubsysPool = length(allSubsystems);

subsysCounts = zeros(size(usubsys));
for i = 1:length(subsysCounts)
    count = sum(strfind_cellarray(allSubsystems,usubsys{i}));
    subsysCounts(i) = count;
end

% Pool reactions within subnetworks and count subsystems
%## Clindamycin Subnetwork (Lachnospiraceae and Barnesiella)##
CSrxns = cell(0,1);
for i = [2 8]
   CSrxns = [CSrxns(:); genera_models{i}.rxns]; 
end
CSurxns = unique(CSrxns);

CSsubsys = cell(size(CSurxns));
for i = 1:length(CSurxns)
   index = find(strfind_cellarray(urxns,CSurxns{i}));
   CSsubsys(i) = subsys(index);
end

CSallSubsystems = cell(0,1);
for i = 1:length(CSsubsys)
    curSubsystems = strsplit(CSsubsys{i},'|');
    CSallSubsystems = [CSallSubsystems(:); curSubsystems(:)]; 
end
CStotalSubsysPool = length(CSallSubsystems);

CSsubsysCounts = zeros(size(usubsys));
for i = 1:length(usubsys)
    count = sum(strfind_cellarray(CSallSubsystems,usubsys{i}));
    CSsubsysCounts(i) = count;
end

%## C difficile Subnetwork (C. difficile, Mollicutes,Enterobacteriaceae,Coprobacillus, Enterococcus, Akkermansia, and Blautia)##
CDrxns = cell(0,1);
for i = [2:9]
   CDrxns = [CDrxns(:); genera_models{i}.rxns]; 
end
CDurxns = unique(CDrxns);

CDsubsys = cell(size(CDurxns));
for i = 1:length(CDurxns)
   index = find(strfind_cellarray(urxns,CDurxns{i}));
   CDsubsys(i) = subsys(index);
end

CDallSubsystems = cell(0,1);
for i = 1:length(CDsubsys)
    curSubsystems = strsplit(CDsubsys{i},'|');
    CDallSubsystems = [CDallSubsystems(:); curSubsystems(:)]; 
end
CDtotalSubsysPool = length(CDallSubsystems);

CDsubsysCounts = zeros(size(usubsys));
for i = 1:length(usubsys)
    count = sum(strfind_cellarray(CDallSubsystems,usubsys{i}));
    CDsubsysCounts(i) = count;
end


% Calculate p-values for each subsystem within each subnetwork
%## Clindamycin Subnetwork (Lachnospiraceae and Barnesiella)##
CSpvals = zeros(size(usubsys));
for i = 1:length(usubsys)
    numOfSubsys_i_InOverallPopulation = subsysCounts(i);
    % totalSubsysPool
    numOfSubsys_i_InCSpopulation = CSsubsysCounts(i);
    % CStotalSubsysPool
    CSpvals(i) = sum( hygepdf([numOfSubsys_i_InCSpopulation:numOfSubsys_i_InOverallPopulation ],totalSubsysPool,numOfSubsys_i_InOverallPopulation,CStotalSubsysPool) );
end

%## C difficile Subnetwork (C. difficile, Mollicutes,Enterobacteriaceae,Coprobacillus, Enterococcus, Akkermansia, and Blautia)##
CDpvals = zeros(size(usubsys));
for i = 1:length(usubsys)
    numOfSubsys_i_InOverallPopulation = subsysCounts(i);
    % totalSubsysPool
    numOfSubsys_i_InCDpopulation = CDsubsysCounts(i);
    % CDtotalSubsysPool
    CDpvals(i) = sum( hygepdf([numOfSubsys_i_InCDpopulation:numOfSubsys_i_InOverallPopulation ],totalSubsysPool,numOfSubsys_i_InOverallPopulation,CDtotalSubsysPool) );
end

%---------------------------------------------
% Check enrichment in each genus individually
%---------------------------------------------
allPvals = zeros(length(usubsys),length(genus_names));
for j = 1:length(genus_names)
    % Pool reactions within subnetworks and count subsystems
    INDrxns = cell(0,1);
    for i = [j]
       INDrxns = [INDrxns(:); genera_models{i}.rxns]; 
    end
    INDurxns = unique(INDrxns);

    INDsubsys = cell(size(INDurxns));
    for i = 1:length(INDurxns)
       index = find(strfind_cellarray(urxns,INDurxns{i}));
       INDsubsys(i) = subsys(index);
    end

    INDallSubsystems = cell(0,1);
    for i = 1:length(INDsubsys)
        curSubsystems = strsplit(INDsubsys{i},'|');
        INDallSubsystems = [INDallSubsystems(:); curSubsystems(:)]; 
    end
    INDtotalSubsysPool = length(INDallSubsystems);

    INDsubsysCounts = zeros(size(usubsys));
    for i = 1:length(usubsys)
        count = sum(strfind_cellarray(INDallSubsystems,usubsys{i}));
        INDsubsysCounts(i) = count;
    end


    % Calculate p-values for each subsystem within each subnetwork
    %## Clindamycin Subnetwork (Lachnospiraceae and Barnesiella)##
    INDpvals = zeros(size(usubsys));
    for i = 1:length(usubsys)
        numOfSubsys_i_InOverallPopulation = subsysCounts(i);
        % totalSubsysPool
        numOfSubsys_i_InINDpopulation = INDsubsysCounts(i);
        % INDtotalSubsysPool
        INDpvals(i) = sum( hygepdf([numOfSubsys_i_InINDpopulation:numOfSubsys_i_InOverallPopulation ],totalSubsysPool,numOfSubsys_i_InOverallPopulation,INDtotalSubsysPool) );
    end
    allPvals(:,j) = INDpvals;
end

% HeatMap(allPvals,'RowLabels',usubsys,'ColumnLabels',genus_names)

fid = fopen('enrichment_allPvals.tsv','w');
fprintf(fid,['KEGG_Map_Name' '\t' strjoin(genus_names,'\t') '\t' 'Clind_Sub_Net' '\t' 'Cdiff_Sub_Net' '\n']);
for r = 1:length(usubsys)
    pvals2str = cell(1,length(genus_names));
    for c = 1:length(genus_names)
       pvals2str{c} = num2str(allPvals(r,c)); 
    end
    pvals2str{end+1} = num2str(CSpvals(r));
    pvals2str{end+1} = num2str(CDpvals(r));
    fprintf(fid,[usubsys{r} '\t' strjoin(pvals2str,'\t') '\n']);
end
fclose(fid);