function [seedMetaboliteList, seedMetabolitLogicalVector, ignoredMets] = findSeedMetabolites(cobraModel)
%--------------------------------------------------------------------------
% A "seed set" of metabolites means the union of essential sets of 
% metabolites accross all environments a microbe can find itself in.
% Practice finding a "seed set" as in:
%   Borenstein et al. (2008) Large-scale reconstruction and phylogenetic 
%   analysis of metabolic environments. PNAS, 105(38):14482-14487.
% Input:
% cobraModel = metabolic reconstruction in COBRA format
% Output:
% seedMetaboliteList = cell array of names of metabolites
% seedMetabolitLogicalVector = logical index into full metabolite list in
%                              COBRA model
% ignoredMets = highly-connected metabolites that are not considered 
%               during the analysis
%
% Written by Matt Biggs, mb3ad@virginia.edu, 2013
%--------------------------------------------------------------------------

N = length(cobraModel.mets);
R = length(cobraModel.rxns);
S = cobraModel.S;
metConnectivityCutoff = 370; % I used the script 'testSeedCutoff.m' to determine this threshold
seedMetConfidenceThreshold = 0.2;
%-----------------------------------
% Determine highly-connect metabolites and add to the "ignore" list
%-----------------------------------
metaboliteRxnCounts = zeros(N,1);
mets2ignore = logical(zeros(N,1));
for i = 1:N
    metaboliteRxnCounts(i) = sum(S(i,:) ~= 0);
end
mets2ignore(metaboliteRxnCounts >= metConnectivityCutoff) = 1;
ignoredMets = cobraModel.mets(mets2ignore);
mets2ignore = find(mets2ignore);
%hist(metaboliteRxnCounts(metaboliteRxnCounts < metConnectivityCutoff));
%[char(cobraModel.mets) num2str(metaboliteRxnCounts)]

%-----------------------------------
% Create directed graph of metabolites
%-----------------------------------
graphMat = zeros(N,N);
for r = 1:R
    % ignore exchange reactions
    if sum(S(:,r) ~= 0) > 1
        inputMetIndices = find(S(:,r) < 0);
        outputMetIndices = find(S(:,r) > 0);
        for i = 1:length(inputMetIndices)
            % Proceed only if the metabolite is not on the ignore list
            curInMet = inputMetIndices(i);
            if ~ismember(curInMet,mets2ignore)
               for j = 1:length(outputMetIndices)
                  % Proceed only if the metabolite is not on the ignore list
                  curOutMet = outputMetIndices(j);
                  if ~ismember(curOutMet,mets2ignore)
                      % row contains the originating node
                      % col contains the destination node (edge points here)
                      graphMat(curInMet,curOutMet) = 1;                       
                      if cobraModel.rev(r)
                          graphMat(curOutMet,curInMet) = 1;
                      end
                  end
               end
            end
        end
    end    
end

graph = sparse(graphMat);
%h = view(biograph(graph));

%-----------------------------------
% Find strongly connected components
% Uses MATLAB implementation of Tarjan's SCC Algorithm in Bioinformatics
% toolbox
%-----------------------------------
[numSCCs,SCCassignments] = graphconncomp(graph, 'Directed', true, 'Weak', false);

% Count in-coming edges to each SCC
edgesIn = zeros(numSCCs,1);
for i = 1:numSCCs
   edgesIn(i,1) = sum(sum(graph(SCCassignments ~= i,SCCassignments == i) ));
end
% Count out-going edges from each SCC
edgesOut = zeros(numSCCs,1);
for i = 1:numSCCs
   edgesOut(i,1) = sum(sum(graph(SCCassignments == i,SCCassignments ~= i) ));
end
% Determine size of each SCC
sizeSCCs = zeros(numSCCs,1);
for i = 1:numSCCs
   sizeSCCs(i,1) = sum(SCCassignments == i);
end
% Determine which SCCs:
%   1) Have zero in-coming edges
%   2) Have 1 or more out-going edges OR have more than 10 mets
condition1 = (edgesIn == 0);
condition2 = (edgesOut > 0) + (sizeSCCs > 10);
meetBothConditions = ( (condition1 + condition2) == 2);

D = [(1:numSCCs)' meetBothConditions];   % columns: 'SCC' 'edgesIn'

%-----------------------------------
% Determine "seed set"
%-----------------------------------
seeds = logical(zeros(size(SCCassignments)));
seedSCCs = D(meetBothConditions, 1);
for i = 1:sum(meetBothConditions)
   curSeedSCC = seedSCCs(i);
   numNodesInSCC = sum(SCCassignments==curSeedSCC);
   if (1/numNodesInSCC) >= seedMetConfidenceThreshold
      seeds = logical(seeds + (SCCassignments==curSeedSCC)); 
   end
end

seedMetabolitLogicalVector = seeds;
seedMetaboliteList = sort(cobraModel.mets(seeds));
sprintf('number of SCCs = %d',numSCCs)
sprintf('number of seed mets with compartmentalization = %d',length(seedMetaboliteList))

% remove compartmentalization
metList = regexprep(seedMetaboliteList,'\[.\]',''); 
metList = unique(metList);
seedMetaboliteList = metList;
sprintf('number of seed mets without compartmentalization = %d',length(seedMetaboliteList))
sprintf('number of seed SCCs = %d',sum(meetBothConditions==1))


end