function [GRs,ks,n0s LVGRs] = multiGrowthRates_LV(time,curves)
% Collect the growth rates for a set of curves (each column is a curve)

cols = size(curves,2);

for i = 1:cols
   [r,k,n0,ybestfit] = curve_fit(time,curves(:,i));
   LVGRs(i) = r;
   ks(i) = k;
   n0s(i) = n0;
   [maxGrowthRate,~] = growthCurveMetrics(time,ybestfit); 
   GRs(i) =  maxGrowthRate;   
   plot(time,ybestfit,time,curves(:,i));
end

end