% Calculate SEED set for each genus-level model list
% Calculate competition and mutualism
%
% Written by Matt Biggs, October 2014

load genus_model_list.mat

genus_seed_sets = cell(length(genera_models),1);
numGenera = length(genera_models);

for i = 1:numGenera
    [seedMetaboliteList, seedMetabolitLogicalVector, ignoredMets] = findSeedMetabolites(genera_models{i});
    genus_seed_sets{i} = seedMetaboliteList;
end

genusNames = {'Akkermansia'	'Barnesiella'	'Blautia'	'C_difficile'	'Coprobacillus'	'Enterobacteriaceae'	'Eterococcus'	'Lachnospiraceae'	'Mollicutes'}';

% Verify that seed mets make sense
% Print list of seed metabolites in each genus model
dirName = 'C:\Users\mb3ad\Documents\bitbucket\gut_microbiome_network_analysis\Data';
fid = fopen([dirName '\allGenusSeedSets.txt'],'w');
for i = 1:numGenera
    fprintf(fid,[genusNames{i} '\n']);
    for j = 1:length(genus_seed_sets{i})
        fprintf(fid,[getMetName(genera_models{i},genus_seed_sets{i}{j}) '\n']);
    end
    fprintf(fid,'\n');
end
fclose(fid);

% Competition
numOverlappingSeedMets = zeros(numGenera,numGenera);
for i = 1:numGenera
    for j = 1:numGenera
        geni = genus_seed_sets{i};
        genj = genus_seed_sets{j};
        overlap = intersect(geni,genj);
        numOverlappingSeedMets(i,j) = length(overlap);
    end    
end
numSeedMetsInEachGenera = diag(numOverlappingSeedMets);

% Competition score (fraction of seed set that is shared)
competitionScores = zeros(numGenera,numGenera);
for i = 1:numGenera
    for j = 1:numGenera
        geni = genus_seed_sets{i};
        genj = genus_seed_sets{j};
        overlap = intersect(geni,genj);
        competitionScores(i,j) = length(overlap)/length(geni);
    end    
end

% Mutualism
mutualism = zeros(numGenera,numGenera);
for i = 1:numGenera
    for j = 1:numGenera
        imets = regexprep(genera_models{i}.mets,'\[.\]','');
        geni_mets = setdiff( imets, genus_seed_sets{i});
        geni_seeds = genus_seed_sets{i};
        jmets = regexprep(genera_models{j}.mets,'\[.\]','');
        genj_mets = setdiff( jmets, genus_seed_sets{j});
        genj_seeds = genus_seed_sets{j};
        
        howmuch_i_depends_on_j = intersect(geni_seeds,genj_mets);
        howmuch_j_depends_on_i = intersect(genj_seeds,geni_mets);
        mutualism(i,j) = length(howmuch_i_depends_on_j);    
    end    
end

% Mutualism score
mutualismScore = zeros(numGenera,numGenera);
for i = 1:numGenera
    for j = 1:numGenera
        imets = regexprep(genera_models{i}.mets,'\[.\]','');
        geni_mets = setdiff( imets, genus_seed_sets{i});
        geni_seeds = genus_seed_sets{i};
        jmets = regexprep(genera_models{j}.mets,'\[.\]','');
        genj_mets = setdiff( jmets, genus_seed_sets{j});
        genj_seeds = genus_seed_sets{j};
        
        howmuch_i_depends_on_j = intersect(geni_seeds,genj_mets);
        howmuch_j_depends_on_i = intersect(genj_seeds,geni_mets);
        mutualismScore(i,j) = length(howmuch_i_depends_on_j)/length(geni_seeds);    
    end    
end

