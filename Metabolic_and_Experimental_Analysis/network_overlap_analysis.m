% Find average reaction overlap between genera, and within genera
%
% Written by Matt Biggs, November 2014

load species_list
load genus_model_list

numGenera = length(genera_models);

% Determine how many reactions tend to overlap between species in each genus
ave_overlap_within_genus = zeros(numGenera,1);
ave_model_sizes = zeros(numGenera,1);
num_species = zeros(numGenera,1);
genusNames = cell(0,0);
for i = 1:numGenera
   numSpecies = length(species_models{i});
   genusNames{i,1} = species_models{i}{1}.genus;
   overlap = [];
   model_size = zeros(numSpecies,1);
   for j = 1:numSpecies
       model_size(j) = length(species_models{i}{j}.rxns);
       for k = (j+1):numSpecies
           rxnsj = species_models{i}{j}.rxns;
           rxnsk = species_models{i}{k}.rxns;
           rxn_overlap = sum(ismember(rxnsj,rxnsk));
           overlap(end+1,1) = rxn_overlap;
       end
   end
   num_species(i) = numSpecies;
   ave_model_sizes(i) = mean(model_size);
   ave_overlap_within_genus(i) = mean(overlap) / mean(model_size);
end

figure(1)
plot(num_species,ave_model_sizes,'ro')
xlabel('Number of species in genus')
ylabel('Average model size of species in genus')
figure(2)
plot(num_species,ave_overlap_within_genus,'ro')
xlabel('Number of species in genus')
ylabel('Average overlap between species networks')

% Determine how many unique reactions each species has (within a genus)
ave_unique_within_genus = zeros(numGenera,1);
for i = 1:numGenera
   numSpecies = length(species_models{i});
   uniqueRxns = zeros(numSpecies,1);
   model_size = zeros(numSpecies,1);
   for j = 1:numSpecies
       model_size(j) = length(species_models{i}{j}.rxns);
       rxnsj = species_models{i}{j}.rxns;
       otherRxns = cell(0,0);
       for k = 1:numSpecies
           if k ~= j
               rxnsk = species_models{i}{k}.rxns;
               otherRxns = {otherRxns{:} rxnsk{:}}';
           end
       end
       uniqueRxns(j) = sum(~ismember(rxnsj,otherRxns));
   end   
   ave_unique_within_genus(i) = mean(uniqueRxns) / mean(model_size);
end

figure(3)
plot(num_species,ave_unique_within_genus,'ro')
xlabel('Number of species in genus')
ylabel('Average Species Uniqueness')
figure(4)
plot(ave_model_sizes,ave_unique_within_genus,'ro')
xlabel('Average Model Size')
ylabel('Average Species Uniqueness')


% Determine overlap and uniqueness between genus-level models
overlap = zeros(numGenera,numGenera);
overlapList = [];
uniqueRxns = zeros(numGenera,numGenera);
uniqueRxnsList = [];
model_size = zeros(numGenera,1);
for j = 1:numGenera
   model_size(j) = length(species_models{i}{j}.rxns);
   for k = (j+1):numGenera
       rxnsj = species_models{i}{j}.rxns;
       rxnsk = species_models{i}{k}.rxns;
       rxn_overlap = sum(ismember(rxnsj,rxnsk));
       unique2j = sum(~ismember(rxnsj,rxnsk));
       unique2k = sum(~ismember(rxnsk,rxnsj));
       overlap(j,k) = rxn_overlap;
       overlapList(end+1,1) = rxn_overlap;
       uniqueRxns(j,k) = unique2j; % reactions that row has, that column does not have
       uniqueRxns(k,j) = unique2k;
       uniqueRxnsList(end+1,1) = unique2j;
       uniqueRxnsList(end+1,1) = unique2k;
   end
end
ave_overlap_between_genera = mean(overlapList) / mean(model_size);
ave_unique_rxns_in_genera = mean(uniqueRxnsList) / mean(model_size);




