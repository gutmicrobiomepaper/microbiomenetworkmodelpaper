function [maxGrowthRate,doublingTime] = growthCurveMetrics(t,y)

if length(y) < 2 || length(t) ~= length(y)
    error('Input Data Error in growthCurveMetrics');
end

y = log(y);
N = length(y);

dy = diff(y);
dt = diff(t);

growthRates = dy ./ dt;
growthRates = smooth(growthRates,51);

maxGrowthRate = max(growthRates);

doublingTime = log(2)/maxGrowthRate;

end