# Get KEGG map names for all reactions in SEED database

rxnFile = open('model_seed_rxns.tsv','r')
outfile = open('seed_rxns_kegg_map_names.tsv','w')

for line in rxnFile:
	lps = line.strip().split('\t')
	rxnID = lps[0]
	seedSubsys = lps[4]
	keggMapHTML = lps[5]
	
	# Parse through HTML and extract KEGG map names
	kmhps = keggMapHTML.split('<a href=&#39;javascript:addTab(')
	mapNames = []
	for part in kmhps:
		pps = part.split('"')
		if len(pps) >= 3:
			mapNames.append(pps[3])
		elif part.find('None') > -1:
			mapNames.append('None')

	# Write to file
	outfile.write(rxnID + '\t' + seedSubsys + '\t' + '|'.join(mapNames) + '\n')
	
	
rxnFile.close()