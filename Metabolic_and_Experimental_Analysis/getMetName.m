function [metName] = getMetName(genus_model,metID)

metID = regexprep(metID,'\[.\]','');
mets = regexprep(genus_model.mets,'\[.\]','');
meti = ismember(mets,metID);
metName = genus_model.metNames{meti};

end