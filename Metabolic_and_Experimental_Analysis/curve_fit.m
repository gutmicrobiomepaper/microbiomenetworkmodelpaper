function [r,k,n0,ybestfit] = curve_fit(t,y)
% take as input a timecourse, and predict the function
% parameters that produce the best fit to y over t.
%
% Written by Matt Biggs, 2014

x0 = [1;max(y);y(1)];
lb = [0.001;min(y)+0.0001;min(y)];
ub = [100;max(y)*2.6;max(y)-0.0001];

% Create a finer time scale
newT = [];
newT(1) = t(1);
for i = 2:length(t)
    step = ((t(i)-t(i-1))/20);
    intermediates = (t(i-1)+step):step:t(i);
    newT = [newT;intermediates(:)];
end
corresponding_ts = ismember(newT,t);

xopt = fmincon(@COST,x0,[],[],[],[],lb,ub);

r = xopt(1);
k = xopt(2);
n0 = xopt(3);
[~,ybestfit] = ode45(@dydt,newT,n0,[],r,k);
ybestfit = ybestfit(corresponding_ts);

%---------------------------------------------
    % Cost function (sum of squared residuals)
    function [cost] = COST(x)
       rt = x(1);
       kt = x(2);
       n0t = x(3);

       % Integrate
       [~,y_out] = ode45(@dydt,newT,n0t,[],rt,kt);

       % Find the error
       cost = sum((y_out(corresponding_ts)-y).^2);
    end

    % Equation to fit
    function nprime = dydt(t1,n,r,k)
        nprime = r*n*(k - n)/k;
    end
%---------------------------------------------
end
