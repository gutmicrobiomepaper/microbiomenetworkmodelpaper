% Convert SEED models to COBRA models in Matlab
% Group by genus
%
% Written by Matt Biggs, October 2014

dirName = '..\Genera_metabolic_reconstructions';
listing = dir(dirName);
lname = {listing.name}';
lisdir = logical([listing.isdir]');

numGenera = sum(lisdir(3:end));
species_list = cell(numGenera,0);

for i = 3:length(lname)
    if lisdir(i)
       % For each genus:
       subdirName = [dirName '\' lname{i}];
       sublist = dir(subdirName);
       subFileNames = {sublist.name}';
       gl = cell(0,0);
       for j = 3:length(subFileNames)
           curfile = [subdirName '\' subFileNames{j}];
           if strcmp(curfile(end-9:end),'_model.xls')
                % Convert each model to COBRA format
                newFileName = reformat_SEED_xls_model(curfile);
                % Read in each model and store in cell array
                m = d_xls2model_JAB(newFileName);
                m.description = subFileNames{j}(1:end-10);
                m.genus = lname{i};
                gl{end+1} = m;
           end
       end
       species_list{end+1} = gl;
    end
end

save('species_list.mat',species_list);