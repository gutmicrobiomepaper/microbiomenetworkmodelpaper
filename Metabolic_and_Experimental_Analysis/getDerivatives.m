function [derivative1,t1,derivative2,t2] = getDerivatives(y,t)

if size(y) == size(t)
    y = log(y);
    y = smooth(y,51);
    difY = diff(y);
    difT = diff(t);
    t1 = t(1:end-1);
    
    derivative1 = smooth(difY ./ difT,11);

    difD1 = diff(derivative1);
    difT2 = diff(t1);
    t2 = t(1:end-2);
    derivative2 = difD1 ./ difT2;
else
    error('vectors are not of the same size');
end
end