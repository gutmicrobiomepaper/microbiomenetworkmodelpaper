% Convert member species into a generic genus model (union of reactions)
%
% Written by Matt Biggs, October 2014

load species_list.mat

numGenera = length(species_models);
genera_models = cell(numGenera,1);
genus_names = {'Akkermansia' 'Barnesiella' 'Blautia' 'Clostridium_difficile' 'Coprobacillus' 'Enterobacteriaceae' 'Eterococcus' 'Lachnospiraceae' 'Mollicutes'};

for i = 1:numGenera
    gl = species_models{i};
    numSpecies = length(gl);

    % Combine all models within a genus
    mets = cell(0,0);
    reactions = cell(0,0);
    for j = 1:numSpecies
        tmpM = gl{j};
        mets = [mets(:) ; tmpM.mets(:)];
        reactions = [reactions(:) ; tmpM.rxns(:)];
    end
    
    umets = unique(mets);
    urxns = unique(reactions);
    umetNames = cell(size(umets));
    urxnNames = cell(size(urxns));
    
    S = sparse(zeros(length(umets),length(urxns)));
    b = zeros(length(umets),1);
    c = zeros(length(urxns),1);
    lb = zeros(length(urxns),1);
    ub = 1000*ones(length(urxns),1);
    rev = logical(zeros(size(urxns)));
    description = genus_names{i};
    
    for j = 1:numSpecies
        tmpM = gl{j};
        inmets = ismember(umets,tmpM.mets);
        finmets = find(inmets);
        inTmpMets = [];
        for k = 1:sum(inmets)
           inmet = umets(finmets(k));
           tmpModMetIndex = find(ismember(tmpM.mets,inmet));
           inTmpMets = [inTmpMets; tmpModMetIndex];
        end
        inrxns = ismember(urxns,tmpM.rxns);
        finrxns = find(inrxns);
        inTmpRxns = [];
        for k = 1:sum(inrxns)
           inrxn = urxns(finrxns(k));
           tmpModRxnIndex = find(ismember(tmpM.rxns,inrxn));
           inTmpRxns = [inTmpRxns; tmpModRxnIndex];
        end
        S(inmets,inrxns) = tmpM.S(inTmpMets,inTmpRxns);
        lb(inrxns) = tmpM.lb(inTmpRxns);
        ub(inrxns) = tmpM.ub(inTmpRxns);
        umetNames(inmets) = tmpM.metNames(inTmpMets);
        urxnNames(inrxns) = tmpM.rxnNames(inTmpRxns);
        rev(inrxns) = tmpM.lb(inTmpRxns) < 0;
    end
    
    gm.S = S;
    gm.mets = umets;
    gm.metNames = umetNames;
    gm.rxns = urxns;
    gm.rxnNames = urxnNames;
    gm.b = b;
    gm.c = c;
    gm.lb = lb;
    gm.ub = ub;
    gm.rev = rev;
    gm.description = description;
    size(S)    
    genera_models{i} = gm;
end

save('genus_model_list.mat',genera_models);