
1. Escherichia coli
A well-studied enteric bacterium
Kingdom: Bacteria
Chromosome: 1
Plasmids: 6
Genome ID: 167

2. Salmonella enterica
Causes enteric infections
Kingdom: Bacteria
Chromosome: 1
Plasmids: 4
Genome ID: 152

3. Klebsiella pneumoniae
Opportunistic pathogen that causes multiple hospital-acquired infections
Kingdom: Bacteria
Chromosome: 1
Plasmids: 6
Genome ID: 815

4. Yersinia pestis
Causative agent of plague
Kingdom: Bacteria
Chromosome: 1
Plasmids: 4
Genome ID: 153

5. Shigella flexneri
Causes enteric disease
Kingdom: Bacteria
Chromosome: 1
Plasmids: 5
Genome ID: 182

6. Enterobacter
Group of uncharacterized isolates
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 13526

7. Enterobacter cloacae
Nosocomial pathogen
Kingdom: Bacteria
Chromosome: 1
Plasmids: 4
Genome ID: 1219

8. Serratia marcescens
Opportunistic human pathogen
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 1112

9. Klebsiella oxytoca
Causes infectious spondylodiscitis
Kingdom: Bacteria
Chromosome: 1
Plasmids: 4
Genome ID: 1165

10. Buchnera aphidicola
Aphid endosymbiont
Kingdom: Bacteria
Chromosome: 1
Plasmids: 2
Genome ID: 170

11. Escherichia
Group of uncharacterized isolates
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13535

12. Erwinia amylovora
Causative agent of Fire Blight disease in fruit trees
Kingdom: Bacteria
Chromosomes: 8
Plasmids: 2
Genome ID: 1159

13. Yersinia enterocolitica
Causes gastroenteritis
Kingdom: Bacteria
Chromosome: 1
Plasmids: 2
Genome ID: 1041

14. Enterobacter aerogenes
Opportunistic human pathogen
Kingdom: Bacteria
Chromosome: 1
Plasmids: 2
Genome ID: 3417

15. Cronobacter sakazakii
Causes septicemia and enterocolitis in infants
Kingdom: Bacteria
Chromosome: 1
Plasmids: 3
Genome ID: 1170

16. Pantoea
Group of uncharacterized isolates
Kingdom: Bacteria
Chromosome: 1
Plasmids: 5
Genome ID: 13565

17. Serratia
Group of uncharacterized isolates
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 13547

18. Yersinia pseudotuberculosis
Environmental bacterium that causes gastrointestinal disease
Kingdom: Bacteria
Chromosome: 1
Plasmids: 2
Genome ID: 510

19. Klebsiella
Group of uncharacterized isolates
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13510

20. Citrobacter freundii
Citrobacter freundii overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 2850

21. Xenorhabdus bovienii
Insect pathogenic bacterium
Kingdom: Bacteria
Chromosome: 1
Genome ID: 1226

22. Rahnella
Group of uncharacterized isolates
Kingdom: Bacteria
Chromosome: 1
Plasmids: 2
Genome ID: 13529

23. Pantoea ananatis
Pantoea ananatis overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 2606

24. Providencia alcalifaciens
Providencia alcalifaciens overview
Kingdom: Bacteria

Chromosomes:  no data
                
Plasmids: 1
Genome ID: 1997

25. Dickeya zeae
Dickeya zeae overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 1803

26. Shigella dysenteriae
Causes bacillary dysentery
Kingdom: Bacteria
Chromosome: 1
Plasmids: 2
Genome ID: 415

27. Citrobacter
Group of uncharacterized isolates
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13593

28. Pectobacterium carotovorum
Pectobacterium carotovorum overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 1799

29. Proteus mirabilis
Normal gastrointestinal bacterium
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 1162

30. Shigella boydii
Causes dysentery
Kingdom: Bacteria
Chromosome: 1
Plasmids: 5
Genome ID: 496

31. Shigella sonnei
A leading cause of dysentery
Kingdom: Bacteria
Chromosome: 1
Plasmids: 4
Genome ID: 417

32. Dickeya solani
Dickeya solani overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 14829

33. Serratia plymuthica
Serratia plymuthica overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 12998

34. Morganella morganii
Morganella morganii overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 10874

35. Edwardsiella tarda
Edwardsiella tarda overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 2
Genome ID: 1718

36. Dickeya dadantii
Plant pathogen
Kingdom: Bacteria
Chromosome: 1
Genome ID: 944

37. Serratia fonticola
Serratia fonticola overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 17711

38. Dickeya
Dickeya sp. overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 14864

39. Cronobacter dublinensis
Cronobacter dublinensis overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13076

40. Escherichia albertii
Pathogenic enteric bacterium
Kingdom: Bacteria
Chromosome: 1
Genome ID: 1729

41. Yersinia
Yersinia sp. overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 22955

42. Cronobacter pulveris
Cronobacter pulveris overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 22828

43. Dickeya dianthicola
Dickeya dianthicola overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 14821

44. Photorhabdus temperata
Photorhabdus temperata overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 12565

45. Pantoea agglomerans
Pantoea agglomerans overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 10239

46. Cronobacter turicensis
Opportunistic pathogen
Kingdom: Bacteria
Chromosome: 1
Plasmids: 3
Genome ID: 2309

47. Cedecea
Cedecea sp. overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 34037

48. Cronobacter helveticus
Cronobacter helveticus overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 22956

49. Erwinia
Group of uncharacterized isolates
Kingdom: Bacteria
Chromosome: 1
Plasmids: 5
Genome ID: 13524

50. Cronobacter malonaticus
Cronobacter malonaticus overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 12259


51. Erwinia chrysanthemi
Plant pathogen
Kingdom: Bacteria
Chromosome: 1
Genome ID: 11043

52. Hafnia alvei
Enteric bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 10737

53. Enterobacter hormaechei
Enterobacter hormaechei overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 3181

54. Enterobacter asburiae
Enterobacter asburiae overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 2
Genome ID: 2466

55. Escherichia fergusonii
Common fecal bacterium
Kingdom: Bacteria
Chromosome: 1
Plasmids: 5
Genome ID: 1877

56. Pectobacterium wasabiae
Pectobacterium wasabiae overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 1802

57. Salmonella bongori
Reptile-associated bacterium
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 1089

58. Candidatus Moranella endobia
Enterobacteriaceae bacterium PCIT overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 3211

59. Rahnella aquatilis
Rahnella aquatilis overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 3
Genome ID: 3007

60. Tatumella
Tatumella sp. overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 24572

61. Morganella
Morganella sp. overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 17751

62. Serratia liquefaciens
Serratia liquefaciens overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 15879

63. Kosakonia radicincitans
Enterobacter radicincitans overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13444

64. Yokenella regensburgei
Human gastrointestinal tract bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11414

65. Pantoea stewartii
Pantoea stewartii overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2303

66. Candidatus Regiella insecticola
Insect symbiont
Kingdom: Bacteria

Chromosomes:  no data
                
Plasmids: 1
Genome ID: 2295

67. Enterobacter cancerogenus
Enterobacter cancerogenus overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2043

68. Providencia rettgeri
Providencia rettgeri overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 1998

69. Erwinia pyrifoliae
Erwinia pyrifoliae overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 4
Genome ID: 1917

70. Serratia symbiotica
Serratia symbiotica str. 'Cinara cedri'
Kingdom: Bacteria
Chromosome: 1
Genome ID: 1823

71. Candidatus Hamiltonella defensa
Candidatus Hamiltonella defensa overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 1800

72. Edwardsiella ictaluri
Causes enteric septicemia in catfish
Kingdom: Bacteria
Chromosome: 1
Genome ID: 1264

73. Citrobacter rodentium
Causes murine colonic hyperplasia
Kingdom: Bacteria
Chromosome: 1
Plasmids: 3
Genome ID: 1237

74. Xenorhabdus nematophila
Insect pathogenic bacterium
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 1227

75. Photorhabdus luminescens
Bioluminescent bacterium
Kingdom: Bacteria
Chromosome: 1
Genome ID: 1123

76. Pectobacterium atrosepticum
Causative agent for blackleg and soft rot disease in potatoes
Kingdom: Bacteria
Chromosome: 1
Plasmids: 2
Genome ID: 1088

77. Wigglesworthia glossinidia
Tsetse fly endosymbiont
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 1066

78. Providencia stuartii
Normal gastrointestinal bacterium
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 971

79. Kluyvera
Kluyvera sp. overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 34050

80. Arsenophonus endosymbiont of Nilaparvata lugens
Arsenophonus endosymbiont str. Hangzhou of Nilaparvata lugens overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 34009

81. Mangrovibacter
Mangrovibacter sp. overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32118

82. Cronobacter
Genome sequencing of isolates representing seven Cronobacter species
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32032

83. Edwardsiella piscicida
Edwardsiella piscicida overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 31998

84. Lonsdalea quercina
Lonsdalea quercina overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31961

85. Enterobacter massiliensis
Enterobacter massiliensis JC163 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31514

86. Xenorhabdus cabanillasii
Xenorhabdus cabanillasii JM26 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31067

87. Xenorhabdus szentirmaii
Xenorhabdus szentirmaii DSM 16338 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31066

88. Sodalis
Sodalis sp. HS1 overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 30962

89. Cronobacter zurichensis
Cronobacter zurichensis overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30034

90. Kosakonia sacchari
Enterobacter sacchari overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 24134

91. Pantoea dispersa
Pantoea dispersa overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 22561

92. Phaseolibacter flectens
Phaseolibacter flectens overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 17791

93. Enterobacter lignolyticus
Enterobacter lignolyticus overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 17763

94. Plesiomonas shigelloides
Plesiomonas shigelloides overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 17449

95. Erwinia toletana
Erwinia toletana overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 16493

96. Candidatus Blochmannia chromaiodes
Candidatus Blochmannia chromaiodes overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 16016

97. Raoultella ornithinolytica
Raoultella ornithinolytica overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 15885

98. Proteus hauseri
Proteus hauseri overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 15876

99. Dickeya paradisiaca
Dickeya paradisiaca overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 14828

100. Lelliottia amnigena
Enterobacter amnigenus overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 14731


101. Citrobacter sedlakii
Citrobacter sedlakii overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 14649

102. Edwardsiella hoshinae
Edwardsiella hoshinae overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 14624

103. Yersinia massiliensis
Yersinia massiliensis overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 14388

104. Pectobacterium
Group of uncharacterized isolates
Kingdom: Bacteria
Chromosome: 1
Genome ID: 13860

105. Brenneria
Group of uncharacterized isolates
Kingdom: Bacteria
Chromosome: 1
Genome ID: 13743

106. Budvicia aquatica
Budvicia aquatica overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13494

107. Cronobacter condimenti
Cronobacter condimenti overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13179

108. Cronobacter muytjensii
Cronobacter muytjensii overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13099

109. Cronobacter universalis
Cronobacter universalis overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13080

110. Providencia burhodogranariea
Providencia burhodogranariea overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 12508

111. Providencia sneebia
Providencia sneebia overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 3
Genome ID: 12507

112. Escherichia vulneris
Escherichia vulneris overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 12073

113. Escherichia hermannii
Escherichia hermannii overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 12072

114. Citrobacter werkmanii
Citrobacter werkmanii overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 12070

115. Erwinia tracheiphila
Erwinia tracheiphila overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11568

116. Kluyvera ascorbata
Kluyvera ascorbata overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11496

117. Buttiauxella agrestis
Buttiauxella agrestis overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11489

118. Leminorella grimontii
Leminorella grimontii overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11488

119. Raoultella planticola
Raoultella planticola overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11487

120. Tatumella ptyseos
Tatumella ptyseos overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11486

121. Trabulsiella guamensis
Trabulsiella guamensis overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11485

122. Leclercia adecarboxylata
Part of the normal human microbial community
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11413

123. Ewingella americana
Enteric bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11412

124. Arsenophonus nasoniae
Arsenophonus nasoniae overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11261

125. Shimwellia blattae
Escherichia blattae overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 11143

126. Cedecea davisae
Cedecea davisae overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11124

127. Enterobacter mori
Plant pathogen
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 3497

128. Candidatus Blochmannia vafer
Candidatus Blochmannia vafer overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 2971

129. Pantoea vagans
Pantoea vagans overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 3
Genome ID: 2707

130. Serratia odorifera
Serratia odorifera overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2321

131. Klebsiella variicola
Plant associated bacterium
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 2121

132. Providencia rustigianii
Providencia rustigianii overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2042

133. Citrobacter youngae
Citrobacter youngae overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1935

134. Erwinia billingiae
Erwinia billingiae overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 2
Genome ID: 1918

135. Photorhabdus asymbiotica
Photorhabdus asymbiotica overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 1768

136. Yersinia ruckeri
Yersinia ruckeri overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1759

137. Yersinia rohdei
Yersinia rohdei overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1758

138. Yersinia aldovae
Yersinia aldovae overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1757

139. Candidatus Riesia pediculicola
Candidatus Riesia pediculicola overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 1734

140. Yersinia bercovieri
Occasional human pathogen
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1689

141. Erwinia tasmaniensis
Erwinia tasmaniensis overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 5
Genome ID: 1614

142. Serratia proteamaculans
Serratia proteamaculans overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 1459

143. Yersinia intermedia
Yersinia intermedia overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1398

144. Yersinia mollaretii
Yersinia mollaretii overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1397

145. Candidatus Blochmannia pennsylvanicus
Endosymbiont of ants
Kingdom: Bacteria
Chromosome: 1
Genome ID: 1329

146. Citrobacter koseri
Causative agent of neonatal meningitis
Kingdom: Bacteria
Chromosome: 1
Plasmids: 2
Genome ID: 1169

147. Proteus penneri
Normal gastrointestinal bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 970

148. Yersinia kristensenii
Environmental bacterium
Kingdom: Bacteria
Chromosome: 1
Genome ID: 875

149. Yersinia frederiksenii
Environmental bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 873

150. Sodalis glossinidius
An endosymbiont of Glossina, the tsetse fly
Kingdom: Bacteria
Chromosome: 1
Plasmids: 3
Genome ID: 531


151. Enterobacteriaceae bacterium 9_2_54FAA
Enterobacteriaceae bacterium 9_2_54FAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2529

152. Enterobacteriaceae bacterium B14
Enterobacteriaceae bacterium B14 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30234

153. Enterobacteriaceae bacterium LSJC7
Enterobacteriaceae bacterium LSJC7 RefSeq Genome
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30189

154. Enterobacteriaceae bacterium strain FGI 57
Enterobacteriaceae bacterium strain FGI 57 RefSeq Genome
Kingdom: Bacteria
Chromosome: 1
Genome ID: 30177

155. Candidatus Sodalis pierantonius str. SOPE
primary endosymbiont of Sitophilus oryzae overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 11136

156. Tatumella morbirosei
Tatumella morbirosei strain:LMG 23360 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 33991

157. Klebsiella michiganensis
Klebsiella michiganensis strain:SA2 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 33882

158. Pectobacterium betavasculorum
Pectobacterium betavasculorum Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 33811

159. Serratia nematodiphila
Serratia nematodiphila overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32720

160. Sodalis-like symbiont of Philaenus spumarius
Genome sequencing of Sodalis-like endosymbiont of the meadow spittlebug Philaenus spumarius
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31832

161. Erwinia mallotivora
Erwinia mallotivora strain:BT-MARDI Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 24629

162. Yersinia similis
Yersinia similis overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 1
Genome ID: 22952

163. Klebsiella cf. planticola B43
Klebsiella cf. planticola B43 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 16237

164. Citrobacter amalonaticus
Citrobacter amalonaticus overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 15537

165. Pluralibacter gergoviae
Enterobacter gergoviae overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 14770

166. Serratia grimesii
Serratia grimesii overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 14651

167. Cedecea neteri
Cedecea neteri overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 14633

168. secondary endosymbiont of Heteropsylla cubana
secondary endosymbiont of Heteropsylla cubana overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 14109

169. secondary endosymbiont of Ctenarytaina eucalypti
secondary endosymbiont of Ctenarytaina eucalypti overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 14108

170. Proteus vulgaris
Proteus vulgaris overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 10783

171. Plautia stali symbiont
Plautia stali symbiont overview
Kingdom: Bacteria
Chromosome: 1
Plasmids: 2
Genome ID: 3258

172. Candidatus Blochmannia floridanus
Ant endosymbiont
Kingdom: Bacteria
Chromosome: 1
Genome ID: 1115

173. gamma proteobacterium IMCC2047
gamma proteobacterium IMCC2047 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 3050

174. gamma proteobacterium HIMB30
gamma proteobacterium HIMB30 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2813

175. gamma proteobacterium NOR5-3
gamma proteobacterium NOR5-3 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1809

176. marine gamma proteobacterium HTCC2148
marine gamma proteobacterium HTCC2148 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1779

177. gamma proteobacterium HTCC5015
gamma proteobacterium HTCC5015 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1534

178. marine gamma proteobacterium HTCC2080
marine gamma proteobacterium HTCC2080 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1483

179. gamma proteobacterium HTCC2207
gamma proteobacterium HTCC2207 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1310

180. marine gamma proteobacterium HTCC2143
marine gamma proteobacterium HTCC2143 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1309

