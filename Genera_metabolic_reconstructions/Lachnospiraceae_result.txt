
1. Butyrivibrio
Butyrivibrio sp. overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 16211

2. [Clostridium] clostridioforme
Clostridium clostridioforme overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2847

3. Butyrivibrio fibrisolvens
Butyrivibrio fibrisolvens overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 2228

4. Oribacterium
Group of uncharacterized isolates
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13438

5. [Clostridium] bolteae
Normal human gut bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 989

6. Lachnospira multipara
Lachnospira multipara overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 16245

7. Pseudobutyrivibrio ruminis
Pseudobutyrivibrio ruminis overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 16212

8. Coprococcus
Group of uncharacterized isolates
Kingdom: Bacteria
Chromosome: 1
Genome ID: 13745

9. [Clostridium] hathewayi
Clostridium hathewayi overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2080

10. Roseburia intestinalis
Roseburia intestinalis overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 2047

11. Butyrivibrio proteoclasticus
Butyrivibrio proteoclasticum overview
Kingdom: Bacteria
Chromosomes: 2
Plasmids: 2
Genome ID: 1738

12. Lachnoclostridium phytofermentans
Clostridium phytofermentans overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 1402

13. [Clostridium] symbiosum
Normal gastrointestinal bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 981

14. [Ruminococcus] gnavus
Normal gastrointestinal bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 979

15. Oribacterium parvum
Oribacterium parvum ACB8 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31990

16. Pseudobutyrivibrio
Pseudobutyrivibrio sp. overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 22985

17. Blautia wexlerae
Blautia wexlerae overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 16213

18. Lachnobacterium bovis
Lachnobacterium bovis overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 16200

19. Dorea
Group of uncharacterized isolates
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13718

20. Lachnoanaerobaculum saburreum
Eubacterium saburreum overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 3164

21. [Clostridium] saccharolyticum
Clostridium saccharolyticum overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 2541

22. Dorea formicigenerans
Dorea formicigenerans overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2064

23. Dorea longicatena
Normal gut bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 985

24. [Clostridium] scindens
Normal gastrointestinal bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 977

25. [Ruminococcus] torques
Normal gastrointestinal bacterium
Kingdom: Bacteria
Chromosome: 1
Genome ID: 973

26. [Ruminococcus] obeum
Normal gastrointestinal bacterium
Kingdom: Bacteria
Chromosome: 1
Genome ID: 972

27. Lachnospiraceae oral taxon 107
Lachnospiraceae oral taxon 107 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2556

28. [Clostridium] aminophilum
[Clostridium] aminophilum overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32238

29. Oribacterium asaccharolyticum
Oribacterium asaccharolyticum ACB7 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31989

30. [Clostridium] aerotolerans
Clostridium aerotolerans DSM 5434 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31937

31. Robinsoniella
Robinsoniella sp. KNHs210 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31912

32. [Desulfotomaculum] guttoideum
Desulfotomaculum guttoideum DSM 4024 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30952

33. Blautia schinkii
Blautia schinkii overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 18383

34. [Clostridium] indolis
Clostridium indolis overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 17363

35. Lachnoanaerobaculum
Lachnoanaerobaculum sp. overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 16462

36. [Clostridium] methoxybenzovorans
Clostridium methoxybenzovorans overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 16399

37. Butyrivibrio hungatei
Butyrivibrio hungatei overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 16202

38. Blautia
Blautia sp. overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 14940

39. Blautia producta
Blautia producta overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 14364

40. Shuttleworthia
Shuttleworthia sp. overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13967

41. Anaerostipes
Group of uncharacterized isolates
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13727

42. Oribacterium sinus
Oribacterium sinus overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13439

43. Lachnospiraceae bacterium oral taxon 082
Normal human oral flora
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11365

44. Anaerostipes hadrus
Eubacterium hadrum overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 11173

45. [Clostridium] citroniae
Clostridium citroniae WAL-19142 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2848

46. Roseburia hominis
Roseburia hominis overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 2752

47. Coprococcus catus
Coprococcus catus overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 2747

48. Cellulosilyticum lentocellum
Clostridium lentocellum overview
Kingdom: Bacteria
Chromosome: 1
Genome ID: 2467

49. Johnsonella ignava
Johnsonella ignava overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2397

50. Butyrivibrio crossotus
Butyrivibrio crossotus overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2083


51. Roseburia inulinivorans
Roseburia inulinivorans overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2081

52. Blautia hansenii
Blautia hansenii overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2049

53. Tyzzerella nexilis
Clostridium nexile overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2046

54. [Clostridium] hylemonae
Clostridium hylemonae overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2000

55. [Clostridium] asparagiforme
Intestinal tract bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1995

56. Shuttleworthia satelles
Shuttleworthia satelles overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1952

57. Catonella morbi
Catonella morbi overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 1946

58. Coprococcus eutactus
Normal gastrointestinal bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 996

59. Anaerostipes caccae
Normal gastrointestinal bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 984

60. Coprococcus comes
Normal gastrointestinal bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 967

61. Marvinbryantia formatexigens
Normal gastrointestinal bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 957

62. Blautia hydrogenotrophica
Normal gastrointestinal bacterium
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 956

63. Lachnospiraceae bacterium 2_1_46FAA
Lachnospiraceae bacterium 2_1_46FAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2855

64. Lachnospiraceae bacterium 8_1_57FAA
Lachnospiraceae bacterium 8_1_57FAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2372

65. Lachnospiraceae bacterium 3_1_46FAA
Lachnospiraceae bacterium 3_1_46FAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2371

66. Lachnospiraceae bacterium 9_1_43BFAA
Lachnospiraceae bacterium 9_1_43BFAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2352

67. Lachnospiraceae bacterium 7_1_58FAA
Lachnospiraceae bacterium 7_1_58FAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2351

68. Lachnospiraceae bacterium 6_1_63FAA
Lachnospiraceae bacterium 6_1_63FAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2350

69. Lachnospiraceae bacterium 6_1_37FAA
Lachnospiraceae bacterium 6_1_37FAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2349

70. Lachnospiraceae bacterium 5_1_63FAA
Lachnospiraceae bacterium 5_1_63FAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2347

71. Lachnospiraceae bacterium 5_1_57FAA
Lachnospiraceae bacterium 5_1_57FAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2346

72. Lachnospiraceae bacterium 3_1_57FAA_CT1
Lachnospiraceae bacterium 3_1_57FAA_CT1 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2344

73. Lachnospiraceae bacterium 2_1_58FAA
Lachnospiraceae bacterium 2_1_58FAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2343

74. Lachnospiraceae bacterium 1_4_56FAA
Lachnospiraceae bacterium 1_4_56FAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2342

75. Lachnospiraceae bacterium 1_1_57FAA
Lachnospiraceae bacterium 1_1_57FAA overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2341

76. Stomatobaculum longum
Lachnospiraceae bacterium ACC2 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 2947

77. Lachnospiraceae bacterium MA2020
Lachnospiraceae bacterium MA2020 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 33199

78. Lachnospiraceae bacterium AC2029
Lachnospiraceae bacterium AC2029 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32115

79. Lachnospiraceae bacterium FE2018
Lachnospiraceae bacterium FE2018 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32112

80. Lachnospiraceae bacterium AC2028
Lachnospiraceae bacterium AC2028 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32105

81. Lachnospiraceae bacterium AC2012
Lachnospiraceae bacterium AC2012 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32101

82. Lachnospiraceae bacterium C6A11
Lachnospiraceae bacterium C6A11 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32095

83. Lachnospiraceae bacterium FD2005
Lachnospiraceae bacterium FD2005 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32093

84. Lachnospiraceae bacterium ND2006
Lachnospiraceae bacterium ND2006 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32092

85. Lachnospiraceae bacterium P6A3
Lachnospiraceae bacterium P6A3 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32091

86. Lachnospiraceae bacterium MC2017
Lachnospiraceae bacterium MC2017 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32080

87. Lachnospiraceae bacterium AC2014
Lachnospiraceae bacterium AC2014 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 32078

88. Lachnospiraceae bacterium AC2031
Lachnospiraceae bacterium AC2031 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31950

89. Lachnospiraceae bacterium P6B14
Lachnospiraceae bacterium P6B14 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31942

90. Lachnospiraceae bacterium NC2008
Lachnospiraceae bacterium NC2008 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31941

91. Lachnospiraceae bacterium YSB2008
Lachnospiraceae bacterium YSB2008 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31940

92. Lachnospiraceae bacterium MD2004
Lachnospiraceae bacterium MD2004 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31590

93. Lachnospiraceae bacterium AD3010
Lachnospiraceae bacterium AD3010 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31589

94. Lachnospiraceae bacterium V9D3004
Lachnospiraceae bacterium V9D3004 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31588

95. Lachnospiraceae bacterium NC2004
Lachnospiraceae bacterium NC2004 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31586

96. Lachnospiraceae bacterium AB2028
Lachnospiraceae bacterium AB2028 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31578

97. Lachnospiraceae bacterium AC3007
Lachnospiraceae bacterium AC3007 Genome sequencing
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 31577

98. Lachnospiraceae bacterium A2
Lachnospiraceae bacterium A2 RefSeq Genome
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30469

99. Lachnospiraceae bacterium COE1
Lachnospiraceae bacterium COE1 RefSeq Genome
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30468

100. Lachnospiraceae bacterium M18-1
Lachnospiraceae bacterium M18-1 RefSeq Genome
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30467


101. Lachnospiraceae bacterium 28-4
Lachnospiraceae bacterium 28-4 RefSeq Genome
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30466

102. Lachnospiraceae bacterium 3-1
Lachnospiraceae bacterium 3-1 RefSeq Genome
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30465

103. Lachnospiraceae bacterium A4
Lachnospiraceae bacterium A4 RefSeq Genome
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30464

104. Lachnospiraceae bacterium 10-1
Lachnospiraceae bacterium 10-1 RefSeq Genome
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30428

105. Lachnospiraceae bacterium 3-2
Lachnospiraceae bacterium 3-2 RefSeq Genome
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30427

106. Lachnospiraceae bacterium VE202-12
Draft genome sequencing of Lachnospiraceae bacterium VE202-12
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30077

107. Lachnospiraceae bacterium VE202-23
Draft genome sequencing of Lachnospiraceae bacterium VE202-23
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30075

108. Lachnospiraceae bacterium NK4A144
Lachnospiraceae bacterium NK4A144
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30046

109. Lachnospiraceae bacterium NK4A179
Lachnospiraceae bacterium NK4A179
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30010

110. Lachnospiraceae bacterium NK4A136
Lachnospiraceae bacterium NK4A136
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30008

111. Lachnoanaerobaculum sp. MSX33
Lachnospiraceae bacterium MSX33 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13966

112. Lachnospiraceae bacterium JC7
Lachnospiraceae bacterium JC7 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 13227

113. Lachnoanaerobaculum sp. ICM7
Lachnospiraceae bacterium ICM7 overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 12213

114. [Clostridium] celerecrescens
Clostridium celerecrescens overview
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 16393

115. Clostridiales bacterium VE202-29
Draft genome sequencing of Lachnospiraceae bacterium VE202-29
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30800

116. Clostridiales bacterium VE202-27
Draft genome sequencing of Lachnospiraceae bacterium VE202-27
Kingdom: Bacteria

Chromosomes:  no data
                
Genome ID: 30798

