# This method of iterative K means binarization was adapted from Berestovsky et al., 2013
# Genes (species should be columns and timepoints should be rows in csv format)
# Include species names as column labels but no timepoints as row labels 
# e.g.:
#       Sp1 Sp2 Sp3
# time1
# time2
# time3


# run by typing 'python KM_Binarization_iterated.py' in ~/NetworkInfFiles-ForManuscript/BinarizationScripts
# This was run with Python 2.7.6 and the following additional packages. It is recommend to use pip to install
# necessary packages: Pycluster, bitarray, 
import math, numpy, sys
import Pycluster
from bitarray import bitarray
from utilsS2B import Binarization, KarnaughMaps, BestFit
import csv, array

originalSeries = {}	# original series stored here as a list
binarySeries = {}	# structure same as the original but with binary series
inputoutput = {}	# keys are combinations, with a list of compbinations that are outputs of it
#fileName = '../Datasets/combinedInterpolatedSamplesNoClinda_natural'
#fileName = sys.argv[1]

#fileName = '../Datasets/combinedInterpolatedExtrapEquidistantSamplesNoClinda_natural'
inputFileName = '../InterpData/combinedInterpolatedExtrapEquidistantSamplesNoClinda_natural'
outputFileName = '../BinarizedData/1000Binarizations_Equidist2/combinedInterpolatedExtrapEquidistantSamplesNoClinda_natural'

#def decrementalKMeanBinarization(self, orig, clusters, reduction):
def decrementalKMeanBinarization(orig, clusters, reduction):    
    clusterData = {}
    clusterRes = {}
    binSeries = {}
    if clusters < 2:
            return None
    while clusters >= 2:
            # 'NON' allows you to ignore specific species 
            for k,v in orig.items():
                    if k == "NON":
                            continue
                    data = []
                    for i in range(len(v)):
                            row = [i,v[i]]
                            data.append(row)
                    x = numpy.vstack(v)
                    clusterData[k] = x

                    idx, error, nfound = Pycluster.kcluster(x, nclusters=clusters, method='a', dist='e')
                    clusterRes[k] = (idx, error, nfound)

            # replace the values with the averages of the clusters
            for k,r in clusterRes.items():
                    colors = []
                    idx, error, nfound = r
                    y = clusterData[k]
                    x = numpy.arange(len(y))
                    sizes = {}
                    sums = {}
                    for i in range(clusters):
                            sizes[i] = 0
                            sums[i] = 0
                    for i in range(len(idx)):
                            sizes[idx[i]] += 1
                            sums[idx[i]] += y[i]
                    # culculate sums
                    for i in sums.keys():
                            sums[i] /= sizes[i]
                    # replace all the values with sums in the clusters
                    averaged = []
                    for i in idx:
                            averaged.append(sums[i][0])
                    orig[k] = averaged

            clusters /= 2
    # replace data with 0 and 1
    for k,v in orig.items():
            if k == "NON":
                    continue
            mi = min(v)
            binVals = bitarray()
            for i in v:
                    if i == mi:
                            binVals.append(False)
                    else:
                            binVals.append(True)
            binSeries[k] = binVals
    # reduce binary series
    #print "Iterative K means Binarization"
    #print binSeries
    return binSeries
    #if reduction == 0:
    #        reduced = self.reduceBinarySeries(binSeries)
    #        return reduced
    #if reduction == 1:
    #        reduced = self.reduceBinaryXOR(binSeries)
    #return binSeries

counter = 0
while (counter < 1000):
    counter += 1
    print(str(counter) + " of 1000 binarizations")
#def main():
    #binarization method (3 = KM3, 2 = KM2, 1 = KM1)
    bMethod = 3
    #Reduction methods
    reduction = 0
    #input_file = open('genus_abundanceData_transposed.txt', 'r', newline='')
    csvfile = open(inputFileName +'.csv', 'rU') 
    r = csv.reader(csvfile)
    #r = csv.reader(csvfile, dialect='excel-tab')
    #f = open("genus_abundances_crohnsPlusHMP_NoSampleNames.tsv", 'r')
    topLine = r.next()
    order = []
    # grabs each species, 
    for t in topLine:
            # creates an empty list for each species
            originalSeries[t] = []
            order.append(t)
    for line in r:
            if line[0].startswith("#"):
                    continue
            words = line
            # fills the array for each species with the original data points
            for i in range(len(words)):
                    originalSeries[order[i]].append(float(words[i]))
    allConvergence = []
    allTimes = []
    # determines cluster depth (d). Number of clusters, k = 2^d 
    if bMethod > 0:
        clusters = int(math.pow(2,bMethod))
        binarySeries = decrementalKMeanBinarization(dict(originalSeries), clusters, reduction)
    for i in binarySeries:
        sampleSize2= len(binarySeries[i])
    # write binarized data to file
    f = open(outputFileName + str(counter) + '_bin.csv', "wb")
    for item in binarySeries:
        f.write(str(item))
        #print (item + ',')
        #sampleList = list(binarySeries[item])
        values = str(binarySeries[item])
        values  = values.rstrip(')')
        values = values.rsplit('bitarray(')
        values = values[1]
        values = values.lstrip('\'')
        values = values.rstrip('\'')
        valueList = list(values)
        #tmp[1]
        for sample in valueList:
            f.write("," + str(sample))
        #print(str(valueList)+',')
        f.write("\n")

    # Close opened file
    f.close() 
    
#with open('mycsvfile.csv', 'w') as f:  # Just use 'w' mode in 3.x
#    w = csv.DictWriter(f, dict2.keys())
#    for item in dict:
#        w.writer(item)
    #print "sample size: " + str(sampleSize2)
    #print "binarization completed"